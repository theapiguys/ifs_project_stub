<?php
define("BASE_URL", "http://dev.project.com");

/**
 * Infusionsoft Settings
 */
define("IFS_CLIENT_KEY", "");
define("IFS_CLIENT_SECRET", "");
define("TOKEN_STORAGE", __DIR__ . "/token.php");
define("APP_NAME", "");
